import React, { useContext } from 'react';
import Head from 'next/head';
import Nextlink from 'next/link'

import {createTheme} from '@material-ui/core/styles'


import { AppBar, Container, CssBaseline, Link, Switch, ThemeProvider, Toolbar, Typography } from '@material-ui/core';
import useStyles from '../../utils/styles'
import { Store } from '../../utils/store';
import Cookies from 'js-cookie';

const Layout = ({children, title, description}) => {
  const {state, dispatch} = useContext(Store);
  const {darkMode} = state
  const theme = createTheme({
    typography: {
      h1: {
        fontSize: '1.6rem',
        fontWeight: 400,
        margin: '1rem 0',
      },
      h2: {
        fontSize: '1.4rem',
        fontWeight: 400,
        margin: '1rem 0',
      },
      body1: {
        fontWeight: 'normal',
      }
    },
    palette: {
      type: darkMode? 'dark': 'light',
      primary: {
        main: '#F24822'
      },
      secondary: {
        main: '#04B2D9'
      }
    }


  });


  const classes = useStyles();

  const darkModeHandler = () => {
    dispatch({type: darkMode? 'DARK_MODE_OFF' : 'DARK_MODE_ON'})
    const newDarkMode = !darkMode;
    Cookies.set('darkMode', newDarkMode? 'ON': 'OFF');
  }

  return (
    <div>
      <Head>
        <title>{title? `${title} - Next Ecommerce`: 'Next Ecommerce'}</title>
        {description && <meta name="description" content={description}/>}
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppBar className={classes.navbar} position="static">
          <Toolbar>
            <Nextlink href="/" passHref>
              <Link>
                <Typography className={classes.brand}>Ecommerce</Typography>

              </Link>
            </Nextlink>
            <div className={classes.grow}></div>
            <div>
              <Switch checked={darkMode} onChange={darkModeHandler}></Switch>
              <Nextlink href="/cart" passHref>
                <Link>Cart</Link>
              </Nextlink>
              <Nextlink href="/login" passHref>
                <Link>Login</Link>
              </Nextlink>
            </div>
          </Toolbar>
        </AppBar>
        <Container className={classes.main}>
          {children}
        </Container>
        <footer className={classes.footer}>
          <Typography>
            All rights reserverd Next Ecommece
          </Typography>
        </footer>
      </ThemeProvider>

    </div>
  )
}


export default Layout;