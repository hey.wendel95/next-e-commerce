import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  navbar: {
    backgroundColor: '#048ABF',
    '& a': {
      color: '#fff',
      marginLeft: 10
    },
  },

  section: {
    marginTop: 10,
    marginBottom: 10,
  },

  grow: {
    flexGrow: 1
  },

  brand: {
    fontWeight: 'bold',
    fontSize: '1.5rem'
  },

  main: {
    minHeight: '80vh',
  },

  footer: {
    textAlign: 'center',
    marginTop: 10,
  }

});

export default useStyles