import { useRouter } from 'next/router';
import Nextlink from 'next/link'
import Image from 'next/image'

import Layout from '../../components/Layout';


import data from '../../utils/data';
import { Button, Card, Grid, Link, List, ListItem, Typography } from '@material-ui/core';
import useStyles from '../../utils/styles';

// Lessons:
// #1- O dinamismo encontra-se no formato em que o arquivo
//  foi escrito. Ex-> [slug].js. Dessa forma o NextJS entende
//  que esse arquivo tem o valor dinâmico e ao inserir 
//  ele dentro de "pages" obtemos um roteamento dinâmico.
// #2- A partir desse arquivo temos o uso do customHook do Next:
// "useRouter" que cria uma referência com todos os parametros
// de determinada página. Assim podemos usar o conceito de destructuring
// para guardar em "slug" o valor inserido na url. Ex-> .../products/this-slug.
// #3 - É feita uma checagem para filtrar os produtos que existem dentro do mock
// e caso o slug inserido não existir dentro do mock ele retorna que o produto
// não foi encontrado.



const ProductScreen = () => {
  const classes = useStyles();
  const router = useRouter();
  const {slug} = router.query;
  const product = data.products.find(a => a.slug === slug)
  if(!product) {
    return <div>Product not found</div>
  }

  return (
    <Layout title={product.name} description={product.description}>
      <div className={classes.section}>
        <Nextlink href="/" passHref>
          <Link>
          <Typography>
           back to products
          </Typography>
          </Link>
        </Nextlink>
      </div>

      <Grid container spacing={1}>
        <Grid item md={6} xs={12}>
          <Image src={product.image} alt={product.name} width={640} height={640} layout="responsive">

          </Image>
        </Grid>
        <Grid item md={3} xs={12}>
          <List>
            <ListItem>
              <Typography component="h1" variant="h1">
                {product.name}
              </Typography>
            </ListItem>
            <ListItem>
              <Typography>
                Category: {product.category}
              </Typography>
            </ListItem>
            <ListItem>
              <Typography>
                Brand: {product.brand}
              </Typography>
            </ListItem>
            <ListItem>
              <Typography>
                Rating: {product.rating} stars ({product.numReviews})
              </Typography>
            </ListItem>
            <ListItem>              
              <Typography>
                Description: {product.description}
              </Typography>
            </ListItem>
            
          </List>
        </Grid>
        <Grid item md={3} xs={12}>
          <Card>
            <List>
              <ListItem>
                <Grid container>
                  <Grid item xs={6}>
                    <Typography>
                      Price
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>
                      ${product.price}
                    </Typography>
                  </Grid>
                </Grid>
              </ListItem>
              <ListItem>
                <Grid container>
                  <Grid item xs={6}>
                    <Typography>
                      Status
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>
                      {product.countInStock > 0? 'In stock' : 'Unavailable'}
                    </Typography>
                  </Grid>
                </Grid>
              </ListItem>
              <ListItem>
                <Button fullWidth variant="contained" color="primary">
                  Add to cart
                </Button>
              </ListItem>
            </List>
          </Card>
        </Grid>
      </Grid>

    </Layout>
  )
}

export default ProductScreen;